INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('12:00:00', 100, '2023-12-01', 'My first book', 'THRILLER');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('08:47:00', 549, '1952-12-29', 'Another book', 'SCIFI');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('14:48:00', 862, '1964-04-15', 'The Martian', 'SCIFI');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('05:46:00', 880, '1993-09-14', 'About something', 'NON_FICTION');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('02:56:00', 372, '1983-06-10', 'Scary book', 'THRILLER');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('22:59:00', 868, '1995-09-09', 'Lalala', 'SCIFI');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('10:48:00', 105, '1956-04-03', 'Scary book 2', 'THRILLER');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('10:59:00', 412, '1972-04-26', 'Blabla', 'SCIFI');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('14:40:00', 962, '1945-11-01', 'And another', 'NON_FICTION');
INSERT INTO booksschema.books (best_reading_time, max_pages, published, title, genre)
VALUES ('07:58:00', 183, '1972-02-08', 'Fascinating story', 'SCIFI');
