package be.kdg.java2.demojpa.domain;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

@Entity
@Table(name = "BOOKS")
public class Book {
    public enum Genre {
        THRILLER, NON_FICTION, SCIFI
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 50)
    private String title;
    @Column(name = "max_pages")
    private int pages;
    private LocalDate published;
    private LocalTime bestReadingTime;
    @Enumerated(EnumType.STRING)
    private Genre genre;

    //static factory method to generate a random book:
    public static Book randomBook() {
        Random random = new Random();
        return new Book(
                "title" + random.nextInt(),
                random.nextInt(100, 1000),
                LocalDate.ofEpochDay(random.nextLong(-10000, 10000)),
                LocalTime.of(random.nextInt(24), random.nextInt(60)),
                Genre.values()[random.nextInt(Genre.values().length)]);
    }

    protected Book() {
    }

    public Book(String title, int pages, LocalDate published, LocalTime bestReadingTime, Genre genre) {
        this.title = title;
        this.pages = pages;
        this.published = published;
        this.bestReadingTime = bestReadingTime;
        this.genre = genre;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setPublished(LocalDate published) {
        this.published = published;
    }

    public void setBestReadingTime(LocalTime bestReadingTime) {
        this.bestReadingTime = bestReadingTime;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pages=" + pages +
                ", published=" + published +
                ", bestReadingTime=" + bestReadingTime +
                ", genre=" + genre +
                '}';
    }
}
