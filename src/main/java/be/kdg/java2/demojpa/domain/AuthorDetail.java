package be.kdg.java2.demojpa.domain;

import jakarta.persistence.*;

@Entity
@Table(name = "AUTHORDETAILS")
public class AuthorDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String email;

    @OneToOne(mappedBy = "authorDetail")
    private Author author;

    public AuthorDetail() {
    }

    public AuthorDetail(String email) {
        this.email = email;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "AuthorDetail{" +
                "id=" + id +
                ", email='" + email + '\'' +
                '}';
    }
}
