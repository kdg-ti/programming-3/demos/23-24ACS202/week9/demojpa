package be.kdg.java2.demojpa;

import be.kdg.java2.demojpa.domain.Author;
import be.kdg.java2.demojpa.domain.AuthorDetail;
import be.kdg.java2.demojpa.domain.Book;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.Query;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Stream;

@Component
public class DemoRunner implements CommandLineRunner {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void run(String... args) throws Exception {
//        //EntityManager is our "connection" to the database...
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            Book book = new Book("testbook",100, LocalDate.now(),
//                    LocalTime.NOON, Book.Genre.THRILLER);
//            em.getTransaction().begin();
//            em.persist(book);
//            em.getTransaction().commit();
//            System.out.println(book);
//        }
//        //Add 10 books:
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            em.getTransaction().begin();
//            Stream.generate(Book::randomBook).limit(10).forEach(em::persist);
//            em.getTransaction().commit();
//        }
//        //Try to query the database
//        //get me one book:
//        System.out.println("Getting 1 book:");
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            Book book = em.find(Book.class, 5);
//            System.out.println(book);
//        }
//        System.out.println("Getting all books:");
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            List<Book> books = em.createQuery("SELECT b FROM Book b").getResultList();
//            books.forEach(System.out::println);
//        }
//        System.out.println("Getting all thrillers:");
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            List<Book> books = em.createQuery("SELECT b FROM Book b WHERE b.genre = Genre.THRILLER").getResultList();
//            books.forEach(System.out::println);
//        }
//        System.out.println("Adding parameters to the queries:");
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            Query query = em.createQuery("SELECT b FROM Book b WHERE b.genre = :mygenre");
//            query.setParameter("mygenre", Book.Genre.NON_FICTION);
//            List<Book> books = query.getResultList();
//            books.forEach(System.out::println);
//        }
//        System.out.println("Exercise 4:");
//        System.out.println("Maximum pages:");
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            Query query = em.createQuery("SELECT MAX(b.pages) FROM Book b");
//            Integer max = (Integer) query.getSingleResult();
//            System.out.println("Max book pages:" + max);
//        }
//        System.out.println("All books ordered by published date:");
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            List<Book> books = em.createQuery("SELECT b FROM Book b ORDER BY b.published").getResultList();
//            books.forEach(System.out::println);
//        }
//        System.out.println("All books before 1985:");
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            Query query = em.createQuery("SELECT b FROM Book b WHERE b.published < :adate");
//            query.setParameter("adate", LocalDate.of(1985, 1, 1));
//            query.getResultStream().forEach(System.out::println);
//        }
//        //update a book?
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            em.getTransaction().begin();
//            Book book = em.find(Book.class, 5);
//            book.setTitle("A new title for book 5");
//            em.getTransaction().commit();
//        }
//        //update a detached book?
//        Book book;
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            book = em.find(Book.class, 5);
//        }//here em is closed, so all objects become detached...
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            em.getTransaction().begin();
//            book.setTitle("Another title");
//            em.merge(book);//make it managed again
//            em.getTransaction().commit();
//        }
//        //delete a book?
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            em.getTransaction().begin();
//            Book bookToDelete = em.find(Book.class, 5);
//            em.remove(bookToDelete);
//            em.getTransaction().commit();
//        }
//        //delete detached book?
//        Book bookToDelete;
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            bookToDelete = em.find(Book.class, 3);
//        }//here em is closed, so all objects become detached...
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            em.getTransaction().begin();
//            bookToDelete = em.merge(bookToDelete);
//            em.remove(bookToDelete);
//            em.getTransaction().commit();
//        }
//        //Exercise 5: updates and deletes
//        //Set title to uppercase for thrillers
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            em.getTransaction().begin();
//            em.createQuery("update Book b " +
//                    "set title = UPPER(b.title) " +
//                    "where genre = Genre.THRILLER").executeUpdate();
//            em.getTransaction().commit();
//        }
//        //delete all thrillers
//        try (EntityManager em = entityManagerFactory.createEntityManager()){
//            em.getTransaction().begin();
//            em.createQuery("DELETE from Book b " +
//                    "WHERE b.genre = Genre.THRILLER").executeUpdate();
//            em.getTransaction().commit();
//        }
        Author author;
        try (EntityManager em = entityManagerFactory.createEntityManager()) {
            author = new Author("Brusselmans", "Herman");
            AuthorDetail authorDetail = new AuthorDetail("h@gmail.com");
            author.setAuthorDetail(authorDetail);
            em.getTransaction().begin();
            // em.persist(authorDetail);
            em.persist(author);
            em.getTransaction().commit();
        }
        try (EntityManager em = entityManagerFactory.createEntityManager()) {
            //em.getTransaction().begin();
           // em.remove(author);
            System.out.println("Loading the author:");
            Author loadedAuthor = em.find(Author.class, author.getId());
            System.out.println("Getting the details:");
            AuthorDetail authorDetail1 = loadedAuthor.getAuthorDetail();
            System.out.println("Printing it all");
            System.out.println(loadedAuthor);
            System.out.println(authorDetail1);
           // em.getTransaction().commit();
        }
    }
}
